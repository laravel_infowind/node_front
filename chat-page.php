<?php 
include("./common-page/header.php");
?>

<style>
.container{max-width:1170px; margin:auto;}
img{ max-width:100%;}
.inbox_people {
  background: #f8f8f8 none repeat scroll 0 0;
  float: left;
  overflow: hidden;
  width: 40%; border-right:1px solid #c4c4c4;
}
.inbox_msg {
  border: 1px solid #c4c4c4;
  clear: both;
  overflow: hidden;
}
.top_spac{ margin: 20px 0 0;}


.recent_heading {float: left; width:40%;}
.srch_bar {
  display: inline-block;
  text-align: right;
  width: 60%; padding:
}
.headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

.recent_heading h4 {
  color: #05728f;
  font-size: 21px;
  margin: auto;
}
.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
.srch_bar .input-group-addon button {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  padding: 0;
  color: #707070;
  font-size: 18px;
}
.srch_bar .input-group-addon { margin: 0 0 0 -27px;}

.chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
.chat_ib h5 span{ font-size:13px; float:right;}
.chat_ib p{ font-size:14px; color:#989898; margin:auto}
.chat_img {
  float: left;
  width: 11%;
}
.chat_ib {
  float: left;
  padding: 0 0 0 15px;
  width: 88%;
}

.chat_people{ overflow:hidden; clear:both;}
.chat_list {
  border-bottom: 1px solid #c4c4c4;
  margin: 0;
  padding: 18px 16px 10px;
}
.inbox_chat { height: 550px; overflow-y: scroll;}

.active_chat{ background:#ebebeb;}

.incoming_msg_img {
  display: inline-block;
  width: 6%;
}
.received_msg {
  display: inline-block;
  padding: 0 0 0 10px;
  vertical-align: top;
  width: 92%;
 }
 .received_withd_msg p {
  background: #ebebeb none repeat scroll 0 0;
  border-radius: 3px;
  color: #646464;
  font-size: 14px;
  margin: 0;
  padding: 5px 10px 5px 12px;
  width: 100%;
}
.time_date {
  color: #747474;
  display: block;
  font-size: 12px;
  margin: 8px 0 0;
}
.received_withd_msg { width: 57%;}
.mesgs {
  float: left;
  padding: 30px 15px 0 25px;
  width: 60%;
}

 .sent_msg p {
  background: #aacbe9 none repeat scroll 0 0;
  border-radius: 3px;
  font-size: 14px;
  margin: 0; color:#fff;
  padding: 5px 10px 5px 12px;
  width:100%;
}
.outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
.sent_msg {
  float: right;
  width: 46%;
}
.input_msg_write input {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  color: #4c4c4c;
  font-size: 15px;
  min-height: 48px;
  width: 100%;
}

.type_msg {border-top: 1px solid #c4c4c4;position: relative;}
.msg_send_btn {
  background: #05728f none repeat scroll 0 0;
  border: medium none;
  border-radius: 50%;
  color: #fff;
  cursor: pointer;
  font-size: 17px;
  height: 33px;
  position: absolute;
  right: 0;
  top: 11px;
  width: 33px;
}
.messaging { padding: 0 0 50px 0;}
.msg_history {
  height: 516px;
  overflow-y: auto;
}

Here is very simple snippet, without strange modyfing of js - pure and simple css (with "Font Awesome")

$('.select2[multiple]').select2({
    width: '100%',
    closeOnSelect: false
})
#body{
padding: 30px
}

.select2-results__options[aria-multiselectable="true"] li {
    padding-left: 30px;
    position: relative
}

.select2-results__options[aria-multiselectable="true"] li:before {
    position: absolute;
    left: 8px;
    opacity: .6;
    top: 6px;
    font-family: "FontAwesome";
    content: "\f0c8";
}

.select2-results__options[aria-multiselectable="true"] li[aria-selected="true"]:before {
    content: "\f14a";
}
span#is_typing {
    color: green;
}
 input.write_msg {  
                text-align: right; 
 } 
 
 .user-online {
    position: absolute;
    width: 15px;
    height: 15px;
    border-radius: 50%;
    /* bottom: -4px; */
    /* right: 6px; */
    border: 3px solid #fff;
    background: green;
}

.uread-message-chat {
    display: block;
    background: #202286;
    color: #fff;
    display: block;
    width: 20px;
    z-index: 1;
    height: 20px;
    font-size: 12px;
    line-height: 20px;
    border-radius: 50%;
    right: -11px;
    text-align: center;
}

.button {
    float: right;
    border: none;
    width: 50px;
    padding: 12px 0;
    cursor: pointer;
    background: #32465a;
    color: #f5f5f5;
}

.attachment {
    position: absolute;
    right: 60px;
    z-index: 4;
    margin-top: 10px;
    font-size: 1.1em;
    color: #435f7a;
    opacity: .5;
    cursor: pointer;
}
.message_input {
    font-family: "proxima-nova", "Source Sans Pro", sans-serif;
    float: left;
    border: none;
    width: calc(100% - 90px);
    padding: 11px 32px 10px 8px;
    font-size: 0.8em;
    color: #32465a;
}
.input-group {
    position: relative;
   
    display: flex;
   
    align-items: stretch;
    width: 100%;
}
.send_btn {
    background-image: linear-gradient(90deg, #0e79be, #30aeff);
    color: #fff;
}
.input-group-append span.input-group-text {
    height: 45px;
    width: 45px;
    display: flex;
    border-radius: 50% !important;
    background: #f4f4f4;
    align-items: center;
    justify-content: center;
    color: #8097a0;
    cursor: pointer;
}
.input-group-append span.input-group-text {
    position: relative;
    overflow: hidden;
}
textarea {
    margin: 0;
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
input[type="file" i] {
    appearance: initial;
    background-color: initial;
    cursor: default;
    align-items: baseline;
    color: inherit;
    text-align: start !important;
    padding: initial;
    border: initial;
}
.input-group-text input#images {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    cursor: pointer;
    opacity: 0;
}
i.emoji-picker-icon.emoji-picker.fa.fa-smile-o {
    margin-right: 71px;
    margin-top: 10px;
}

</style>
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

</head>
<body>
<div id="profile_div" class="">
<h3>Login User Profile</h3>
 <div class="chat_people">
 <div class="chat_img" id="profile_image"> 
 <img style="height:60px;width:75px" id="user_profile" src="" alt=""> 
  <div class="chat_ib" id="user_name">
 </div>
 </div>

 </div>
 </a>
 <form>
   <button type="button" class="btn btn-link btn-logout">Logout</button>
</form>
 </div>

<style>
div#profile_div {
    margin-left: 101px;
    margin-top: 52px;
}
</style>
<div class="container">

<h3 class=" text-center">Messaging</h3>
<div class="messaging">
      <div class="inbox_msg">
        <div class="inbox_people">
          <div class="headind_srch">
            <div class="recent_heading">
              <h4>Conversation List</h4>
            </div>
			<div class="recent_heading">
              <button type="button" onclick="openModel()"> <i class="fa fa-plus" aria-hidden="true"></i> </button>
            </div>
            <div class="srch_bar">
              <div class="stylish-input-group">
                <p class="lead emoji-picker-container">
                <input type="text" class="search-bar" onkeyup="getConnectedUserList()"  placeholder="type message" >
				</a>
                <span class="input-group-addon">
                <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                </span> </div>
            </div>
          </div>
          <div class="inbox_chat">
            
            
            
          </div>
        </div>
        <div class="mesgs">
          <div class="msg_history">
        
          </div>
		  <div id="image_preview" >
		  </div>
		  
            <div class="type_msg">
		    <form id="send_message_form">
		    <div class="input-group">
					<input type='text' data-emojiable="true" onkeyup="tellForTyping()" class="write_msg message_input" placeholder="Type your message..." />
					<span id="is_typing" style="display:none">is typing..........</span>
					<div class="input-group-append">
						<span onclick="sendMessage()" class="input-group-text send_btn"><i class="fa fa-paper-plane"></i></span>
					</div>
					<div class="input-group-append">
						<span class="input-group-text attach_btn"><i class="fa fa-paperclip"></i>
							<input id="images"  id="uploadFile" type="file" name="chat_attachment" value="" multiple="">
						</span>
					</div>
             </div>
             <span id="message-error" class="invalid-feedback alert-danger" style="display: none;">The message field is required.</span> 
            </form>
		   </div>
		</div>
       </div>
       <input  type="hidden" id="message_room" value="">
       <input  type="hidden" id="room" value="">
	   <input  type="hidden" id="to_user" value="">
	   <input  type="hidden" id="login_user" value="">
      
    </div>
	</div>
	
	
<div class="modal" id="modalSubscriptionForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Group</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <form id="group_form">
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
          <i class="fas fa-group prefix grey-text"></i>
          <input type="text" placeholder="enter group name" name="group" id="group" class="form-control validate">
        </div>

        <div class="md-form mb-4">
          <i class="fas fa-user prefix grey-text"></i>
		  <div id="body">
          <select name="users[]"  multiple="multiple" data-live-search="true" id="user-list" class="form-control select2">
		    
		  </select>
		  </div>
        </div>

      </div>
	  </form>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-indigo" onclick="saveGroup()">Save <i class="fas fa-paper-plane-o ml-1"></i></button>
      </div>
    </div>
  </div>
</div>


	<?php
	
	$initiateRoomId = isset($_GET['room_id'])?$_GET['room_id']:'';
	$initiateRoom = isset($_GET['room'])?$_GET['room']:'';
	$toUser = isset($_GET['to_user'])?$_GET['to_user']:'';
	
	?>
	<script>
	
	var firstRoomId ;
	var firstRoom ;
	var myRooms = [];
	var loginUserId;
	$(document).ready(function(){
		
		$('.select2[multiple]').select2({
        width: '100%',
        closeOnSelect: false
        })
	    loginUserId = localStorage.getItem('userId');
		$('#login_user').val(loginUserId);
		getAllRooms(loginUserId)
		setTimeout(function(){
		 getConnectedUserList(loginUserId);	
		},500);
		setTimeout(function(){
		 getUserMessage(loginUserId,firstRoomId,firstRoom);	
		},1000);
		joinRoom(loginUserId)
		var profile = localStorage.getItem('loginUserProfile');
	    var name = localStorage.getItem('loginUserName');
	    $('#user_profile').attr("src",profile);
	    $('#user_name').html('<h5 >'+name+'</h5>')
	});

    function openModel(){
		$.ajax({
            url:baseUrl+"chat/user-list" ,
			data:{user_id :localStorage.getItem('userId'),search:$('.search-bar').val()},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
				$('#user-list').html('');
				var str = "<option value=''>Select User</option>";
				for (var i=0;i<result.data.length; i++){
					str = str+ "<option onclick='addUsers("+this.value+")' value='"+result.data[i].id+"'>"+result.data[i].name+"</option>"
              
				}
				$('#user-list').html(str);
		    }
        });
		$('#modalSubscriptionForm').show();
	}
	
	var users = [];
	function addUsers(userId){
		users.push(userId)
		console.log(users);
	}
	
	function saveGroup(){
		
		var users = $('#user-list').val();
		var loginUserId = localStorage.getItem('userId');
		users.push(loginUserId)
		console.log(users)
		$.ajax({
            url:baseUrl+"chat/create-room" ,
			data:{group:$('#group').val(),"userId" : JSON.stringify(users),"type":"group"},
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
				$('.write_msg').val('Welcome to this Group')
				$('#message_room').val(result.data.id)
				myRooms.push(result.data.id)
				$('#to_user').val(0)
				sendMessage();
				$('.msg_history').html('');
				getConnectedUserList(loginUserId);
				$('#modalSubscriptionForm').hide();
			
            }
            
        })
	}
	
	$('.close').on('click',function(){
		$('#modalSubscriptionForm').hide();
	});

	$('#send_message_form').keydown(function(e){
		if (e.keyCode == 13 && !e.shiftKey)
		{
		  e.preventDefault();
		 // $('.outgoing_msg').trigger('clcik');
		//  $('.write_msg').trigger('clcik');
		  //$('.write_msg').css({'cursor' :"default"});
		  $('.write_msg').off('focus');
		  $('.write_msg').css({'cursor':"pointer"});
		  sendMessage()
		  //$('.send_btn').trigger('click');
		  return false;
		}
	});

	function joinRoom(loginUserId){
		$.ajax({
            url:baseUrl+"chat/my-rooms" ,
			data:{"user_id" : loginUserId},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
              console.log(result);
			  console.log(socket);
			  socket.emit('new-user', result.data, loginUserId)
			
            }
            
        })
	}
	
	function getAllRooms(loginUserId){
	   $.ajax({
            url:baseUrl + "chat/room-array" ,
			data:{user_id :loginUserId},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
				myRooms = result.data;
			}
        })
	}
	
	function getConnectedUserList(loginUserId,roomId)
	{
	   $.ajax({
            url:baseUrl+"chat/connected-user-list" ,
			data:{user_id :$('#login_user').val(),search:$('.search-bar').val(),rooms:JSON.stringify(myRooms)},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
				$('.inbox_chat').html('');
				if(result.data.length > 0){
				firstRoomId = result.data[0].room_id;
				var str = '';
				for (var i=0;i<result.data.length; i++){
					 var room = "`"+result.data[i].room.room_id+"`";
					 var toUserName = (result.data[i].toUser != null || result.data[i].to_user != 0)?((result.data[i].user.id == $('#login_user').val())?result.data[i].toUser.name:result.data[i].user.name):result.data[i].room.group_name;
					 var toProfile = (result.data[i].toUser != null || result.data[i].to_user != 0)?result.data[i].toUser.profile:BASEURL+'/node_chat//app/controllers/images/group_image.png';
					 firstRoom = result.data[0].room.room_id;
					 var onlineClass = (result.data[i].to_user != 0)?'user-online':'';
					 var onlineDisplay = (result.data[i].toUser && result.data[i].toUser.is_online == 'online')?'':'none';
					 var unraedCount = (result.data[i].unreadCount != 0)?"<span id='count_"+result.data[i].room_id+"' class='uread-message-chat'>"+result.data[i].unreadCount+"</span>":''
					 str = str + " <div id = 'connect_row_"+result.data[i].room_id +"' class='chat_list'><input type='hidden' value='"+result.data[i].to_user+"' id='to_user"+result.data[i].room_id+"'>"+
						   "<a href='javascript:void(0)' class='remove-url' onclick = 'getUserMessage("+loginUserId +","+result.data[i].room_id+","+ room +",`reset`)'>  "+
							 "<div class='chat_people'>"+
							 "<div class='chat_img'> <img src='"+toProfile+"' alt='sunil'> </div><span style='display:"+onlineDisplay+"' id='active"+result.data[i].to_user+"' class="+onlineClass+" ></span>"+
							 "<div class='chat_ib'>"+
							 "<h5>"+toUserName+"<span class='chat_date'>"+result.data[i].created_at+"</span></h5>"+
							 "<p>"+result.data[i].message +unraedCount+"</p>"+
							 "<span id='is_typing"+result.data[i].room_id+"' style='display:none'>is typing..........</span>"+
							"</div></div></a></div>";
              
				}
				$('.inbox_chat').html(str);
				$('#connect_row_'+firstRoomId).addClass('active_chat');
				// getUserMessage(loginUserId,firstRoomId,firstRoom)
				}else{
					$('.inbox_chat').html('<center><h3>No Conversation Found.</h3></center>');
				}
            }
            
        });
	}
	
	function getUserMessage(loginUserId,firstRoomId,room,reset)
	{
		var initiateRoomId = '<?php echo $initiateRoomId ?>';
		var initiateRoom = '<?php echo $initiateRoom ?>';
		if(initiateRoomId.length != 0){
		firstRoomId = initiateRoomId;
		}
		if ((window.location.href.indexOf('?') > -1) && (initiateRoom.length != 0 ) && (reset=='reset')) {
                       window.location.href = window.location.pathname;
        }
		 console.log('room'+room)
		 $('#message_room').val(firstRoomId);
		 $('#room').val(room);
		 $('#to_user').val($('#to_user'+firstRoomId).val())
		 $.ajax({
            url:baseUrl+"chat/user-message" ,
			data:{user_id :$('#login_user').val(),room_id:firstRoomId},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
				$('.msg_history').html('');
				var str = '';
				if(result.data.length > 0){
				var allMessages = result.data.reverse();
				for (var i=0;i < allMessages.length; i++){
					if(result.data[i].user_id != loginUserId){
						if(allMessages[i].files.length != 0){
							for(j=0;j< allMessages[i].files.length;j++){
								  str = str +" <div class='incoming_msg'>"+
								   "<p><strong>"+result.data[i].user.name+"</strong></p>"+
								   "<div class='incoming_msg_img'> <img src='"+result.data[i].user.profile+"' alt='sunil'> </div>"+
								   "<div class='received_msg'>"+
									"<div class='received_withd_msg'>"+
									"<div style='width:100%;height:100px;'>"+
									"<a target='_blank' href='"+allMessages[i].files[j].url+"' >"+allMessages[i].files[j].name+
									"</a><span>"+allMessages[i].files[j].size+"</span><img src='"+BASEURL+"/node_chat//app/controllers/images/image_icon.png'>"+
									"</div>"+
									"<span class='time_date'> "+result.data[i].created_at+"</span></div></div></div>" ;
							}
						}
						if(result.data[i].message){
								str = str + " <div class='incoming_msg'>"+
								 "<p><strong>"+result.data[i].user.name+"</strong></p>"+
							   "<div class='incoming_msg_img'> <img src='"+result.data[i].user.profile+"' alt='sunil'> </div>"+
							   "<div class='received_msg'>"+
								"<div class='received_withd_msg'>"+
								  "<p>"+result.data[i].message+"</p>"+
							   "<span class='time_date'> "+result.data[i].created_at+"</span></div></div></div>" ;
						}
					}else{
						if(allMessages[i].files.length != 0){
							 for(j=0;j< allMessages[i].files.length;j++){
						        str = str + "<div class='outgoing_msg'>"+
						        "<div class='sent_msg'>"+
						        "<div style='width:100%;height:100px;'>"+
						        "<a target='_blank' href='"+allMessages[i].files[j].url+"'>"+allMessages[i].files[j].name+
						        "</a></br><span>"+allMessages[i].files[j].size+"</span><img src='"+BASEURL+"/node_chat//app/controllers/images/image_icon.png'>"+
						        "</div></br>"+
							    "<span class='time_date'>"+result.data[i].created_at+"</span> </div></div>";
						  }
						}
						if(result.data[i].message){
							  str = str + "<div class='outgoing_msg'><div class='sent_msg'><p>"+result.data[i].message+"</p>"+
						      "<span class='time_date'>"+result.data[i].created_at+"</span> </div></div>"; 
						}
				   };
				}
				$('.msg_history').html(str);
				$('.chat_list').removeClass('active_chat');
				$('#connect_row_'+firstRoomId).addClass('active_chat');
				$(".msg_history").animate({ scrollTop: 7000 }, "slow");
				$('#count_'+allMessages[0].room_id).hide();
				}
		    }
        });
	}
	
    var uploadedFiles = [];
	function readURL(input) {
		var files = input.files;
	  for(var i =0;i< files.length; i++) {
		uploadedFiles.push(files[i])  
		var reader = new FileReader();
		var fileType = files[i].type;
		var fileSize = bytesToSize(files[i].size);
		var fileName = files[i].name;
		reader.readAsDataURL(files[i]); 
		reader.onload = function(e) {
			if(fileType.includes("image")){
		      var img = "<div  style='width:100px;height:100px;float:left'><a title='Delete' class='del_img remove_file_"+fileName+"' href='javascript:void(0)' onclick='deleteFile(`"+fileName+"`)'><i class='fa fa-times'></i></a><img   src='"+BASEURL+"/node_chat//app/controllers/images/image_icon.png'  />"+
			  "<p style='font-size:10;font-weight:100;'>"+fileName+" "+ fileSize+"</p>"+
			  "</div>";
			}else{
			  var img = "<div style='width:100px;height:100px;float:left'><a title='Delete' class='del_img remove_file_"+fileName+"' href='javascript:void(0)' onclick='deleteFile(`"+fileName+")`'><i class='fa fa-times'></i></a><img   src='"+BASEURL+"/node_chat//app/controllers/images/doc_icon.png'  />"+
			  "<p style='font-size:10;font-weight:100;'>"+fileName+" "+ fileSize+"</p></div>";
			}
		  $('#image_preview').append(img);
		}
	   }
	}
	
	function deleteFile(fileName){
		alert(fileName)
	}
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Byte';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    }

	$("#images").change(function() {
	  readURL(this);
	});

	function sendMessage(){
		console.log('send message')
		var files = [];
		var data = new FormData();
		for (var i = 0; i < uploadedFiles.length; i++) {
          data.append('files[]', uploadedFiles[i]);
        }
		var loginUserId = localStorage.getItem('userId');
		var message = $('.write_msg').val().trim();
		console.log(message)
		if((uploadedFiles.length == 0) &&  (message == '')){
			$('#message-error').show();
		}else{
			$('#message-error').hide();
			var toUser = '<?php echo $toUser ?>'
			if(toUser.length == 0){
				toUser = $('#to_user').val();
			}
			data.append('from_user', loginUserId);
		    data.append('room_id', $('#message_room').val());
		    data.append('message', $('.write_msg').val());
		    data.append('to_user', toUser);
			
			$.ajax({
            url:baseUrl+"chat/send-message" ,
			cache: false,
		    contentType: false,
		    enctype: 'multipart/form-data',
		    processData: false,
			data:data,
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
				var roomName = $('#room').val();
				var message = $('.write_msg').val();
				socket.emit('send-chat-message', roomName, result.data)
				$('.write_msg').val('');
				$('#image_preview').html('');
				$('.emoji-wysiwyg-editor').html('');
				uploadedFiles = [];
				    var str = '';
					if(result.data.message){
					 str = "<div class='outgoing_msg'><div class='sent_msg'><p>"+result.data.message+"</p><span class='time_date'>"+result.data.created_at+"</span> </div></div>";
					}
					if(result.data.files.length != 0){
							 for(j=0;j< result.data.files.length;j++){
						        str = str + "<div class='outgoing_msg'>"+
						        "<div class='sent_msg'>"+
						        "<div style='width:100%;height:100px;'>"+
						        "<a target='_blank' href='"+result.data.files[j].url+"'>"+result.data.files[j].name+
						        "</a></br><span>"+result.data.files[j].size+"</span><img src='"+BASEURL+"/node_chat//app/controllers/images/image_icon.png'>"+
						        "</div></br>"+
							    "<span class='time_date'>"+result.data.created_at+"</span> </div></div>";
						  }
				  }
				$('.msg_history').append(str);
				$(".msg_history").animate({ scrollTop: 7000 }, "slow");
				  if (window.location.href.indexOf('?') > -1) {
                       window.location.href = window.location.pathname;
                }
				getConnectedUserList(loginUserId);
				$('#count_'+firstRoomId).hide();
            }
        });
		}
	}
	
	function tellForTyping(){
		console.log('typing')
		socket.emit('is_typing', $('#message_room').val(), localStorage.getItem('loginUserName'),loginUserId)
	}
	
	socket.on('online', data => {
		console.log(data)
		$('#active'+data.user_id).show();
	});
		
	socket.on('typing', data => {
		if(myRooms.includes(parseInt(data.room)) && (parseInt(data.user_id) != loginUserId) && ($('#message_room').val()==data.room)){
			$('#is_typing').show();
			$('#is_typing').text(data.name+'  is typing ......')
			setTimeout(function(){
				$('#is_typing').hide();
			},500)
		}
		if(myRooms.includes(parseInt(data.room)) && (parseInt(data.user_id) != loginUserId) && ($('#message_room').val()!=data.room)){
			$('#is_typing'+data.room).show();
			$('#is_typing'+data.room).text(data.name+'  is typing ......')
			setTimeout(function(){
				$('#is_typing'+data.room).hide();
			},500)
		}
	})

	socket.on('chat-message', data => {
		getAllRooms(loginUserId)
		if(myRooms.includes(data.message.room_id) && (data.message.user_id != loginUserId)){
		 var str = '';
		    str = str + "<div class='incoming_msg'>"+
						"<p><strong>"+data.message.user.name+"</strong></p>"+
					    "<div class='incoming_msg_img'> <img src='"+data.message.user.profile+"' alt='sunil'> </div>"+
					    "<div class='received_msg'>"+
						"<div class='received_withd_msg'>"+
						"<p>"+data.message.message+"</p>"+
					    "<span class='time_date'> "+data.message.created_at+"</span></div></div></div>" ;
	    	           if(data.message.files.length != 0){
							for(j=0;j< data.message.files.length;j++){
								  str = str +" <div class='incoming_msg'>"+
								   "<p><strong>"+data.message.user.name+"</strong></p>"+
								   "<div class='incoming_msg_img'> <img src='"+data.message.user.profile+"' alt='sunil'> </div>"+
								   "<div class='received_msg'>"+
									"<div class='received_withd_msg'>"+
									"<div style='width:100%;height:100px;'>"+
									"<a target='_blank'  href='"+data.message.files[j].url+"' >"+data.message.files[j].name+
									"</a><span>"+data.message.files[j].size+"</span><img src='"+BASEURL+"/node_chat//app/controllers/images/image_icon.png'>"+
									"</div>"+
									"<span class='time_date'> "+data.message.created_at+"</span></div></div></div>" ;
							}
		               }
		$('.msg_history').append(str); 
		$(".msg_history").animate({ scrollTop: 7000 }, "slow");
		getConnectedUserList(loginUserId)
		setTimeout(function(){
			if(firstRoomId == data.message.room_id){
		     $('#count_'+firstRoomId).hide();
		     }
		},1000)
		}
	})
	
	$(function() {
    var oTop = $('body').offset().top - window.innerHeight;
    $('.msg_history').scroll(function(){

			var pTop = $('.msg_history').scrollTop();
			console.log( pTop + ' - ' + oTop );   //just for your debugging
			if( pTop == 0 ){
				start_count();
			}
		});
	});

	function start_count(){
		console.log('start_count');
		//Add your code here
	}
	$('.btn-logout').on('click',function(){
		 $.ajax({
            url:baseUrl+"auth/logout" ,
			data:{email :localStorage.getItem('email')},
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
				if(result.success){
					localStorage.clear();
					toastr.success('Logout successfully.');
					setTimeout(function(){
					  window.location = "<?php echo 'index.php' ?>"	
					},1000)
				}else{
					toastr.error(result.message);
				}
		    }
        });
	});
</script>
<script>
      $(function() {
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: './lib/img/',
          popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
      });
    </script>
    <script>
      // Google Analytics
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-49610253-3', 'auto');
      ga('send', 'pageview');
    </script>

<script src="./lib/js/config.js"></script>
<script src="./lib/js/util.js"></script>
<script src="./lib/js/jquery.emojiarea.js"></script>
<script src="./lib/js/emoji-picker.js"></script>
 
<?php 

include("./common-page/footer.php");

?>