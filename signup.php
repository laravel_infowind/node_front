<?php 
include("./common-page/header.php");
?>
	<div class="container h-100">
		<div class="d-flex justify-content-center h-100">
			<div class="user_card">
				<div class="d-flex justify-content-center">
					<div class="brand_logo_container">
						<img src="https://cdn.freebiesupply.com/logos/large/2x/pinterest-circle-logo-png-transparent.png" class="brand_logo" alt="Logo">
					</div>
				</div>
				<div class="d-flex justify-content-center form_container">
					<form id="register_form">
						<div class="input-group mb-3">
							<div class="input-group-append">
								<span class="input-group-text"><i class=""></i></span>
							</div>
							<input type="text" id="email" name="email" class="form-control input_user" value="" placeholder="email">
						</div>
						<div class="input-group mb-2">
							<div class="input-group-append">
								<span class="input-group-text"><i class=""></i></span>
							</div>
							<input type="password"  id="password" name="password" class="form-control input_pass" value="" placeholder="password">
						</div>
						<div class="input-group mb-2">
							<div class="input-group-append">
								<span class="input-group-text"><i class=""></i></span>
							</div>
							<input type="text"  id="name" name="name" class="form-control input_pass" value="" placeholder="name">
						</div>
						<div class="input-group mb-2">
							<div class="input-group-append">
								<span class="input-group-text"><i class=""></i></span>
							</div>
							<input type="file"  id="profile" name="profile" class="form-control input_pass" value="">
						</div>
							<div class="d-flex justify-content-center mt-3 login_container">
				 	<button type="button" name="button" onclick="register()" class="btn login_btn">Register</button>
				   </div>
					</form>
				</div>
				<div class="mt-4">
					<div class="d-flex justify-content-center links">
						Don't have an account? <a href="index.php" class="ml-2">login</a>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
<script>

function register(){
	
	        var data = new FormData();
		 	data.append('email', $('#email').val());
		    data.append('name', $('#name').val());
		    data.append('password', $('#password').val());
			data.append('profile',$('#profile')[0].files[0]);
		 $.ajax({
            url:baseUrl+"auth/register" ,
			cache: false,
		    contentType: false,
		    enctype: 'multipart/form-data',
		    processData: false,
			data:data,
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
				if(result.success){
					toastr.success('Registration successfully.');
					setTimeout(function(){
					 window.location = "<?php echo 'index.php' ?>"	
					},1000);
				}else{
					toastr.error(result.message);
				}
				console.log(result);
            }
            
        });
}

</script>
<style>
	/* Coded with love by Mutiullah Samim */
		body,
		html {
			margin: 0;
			padding: 0;
			height: 100%;
			background: #60a3bc !important;
		}
		.user_card {
			height: 400px;
			width: 350px;
			margin-top: auto;
			margin-bottom: auto;
			background: #f39c12;
			position: relative;
			display: flex;
			justify-content: center;
			flex-direction: column;
			padding: 10px;
			box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			-webkit-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			-moz-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			border-radius: 5px;

		}
		.brand_logo_container {
			position: absolute;
			height: 170px;
			width: 170px;
			top: -75px;
			border-radius: 50%;
			background: #60a3bc;
			padding: 10px;
			text-align: center;
		}
		.brand_logo {
			height: 150px;
			width: 150px;
			border-radius: 50%;
			border: 2px solid white;
		}
		.form_container {
			margin-top: 100px;
		}
		.login_btn {
			width: 100%;
			background: #c0392b !important;
			color: white !important;
		}
		.login_btn:focus {
			box-shadow: none !important;
			outline: 0px !important;
		}
		.login_container {
			padding: 0 2rem;
		}
		.input-group-text {
			background: #c0392b !important;
			color: white !important;
			border: 0 !important;
			border-radius: 0.25rem 0 0 0.25rem !important;
		}
		.input_user,
		.input_pass:focus {
			box-shadow: none !important;
			outline: 0px !important;
		}
		.custom-checkbox .custom-control-input:checked~.custom-control-label::before {
			background-color: #c0392b !important;
		}
</style>
<?php 
include("./common-page/footer.php");
?>