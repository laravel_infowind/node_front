<?php 
include("./common-page/header.php");
?>

<style>
.container{max-width:1170px; margin:auto;}
img{ max-width:100%;}
.inbox_people {
  background: #f8f8f8 none repeat scroll 0 0;
  float: left;
  overflow: hidden;
  width: 40%; border-right:1px solid #c4c4c4;
}
.inbox_msg {
  border: 1px solid #c4c4c4;
  clear: both;
  overflow: hidden;
}
.top_spac{ margin: 20px 0 0;}


.recent_heading {float: left; width:40%;}
.srch_bar {
  display: inline-block;
  text-align: right;
  width: 60%; padding:
}
.headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

.recent_heading h4 {
  color: #05728f;
  font-size: 21px;
  margin: auto;
}
.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
.srch_bar .input-group-addon button {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  padding: 0;
  color: #707070;
  font-size: 18px;
}
.srch_bar .input-group-addon { margin: 0 0 0 -27px;}

.chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
.chat_ib h5 span{ font-size:13px; float:right;}
.chat_ib p{ font-size:14px; color:#989898; margin:auto}
.chat_img {
  float: left;
  width: 11%;
}
.chat_ib {
  float: left;
  padding: 0 0 0 15px;
  width: 88%;
}

.chat_people{ overflow:hidden; clear:both;}
.chat_list {
  border-bottom: 1px solid #c4c4c4;
  margin: 0;
  padding: 18px 16px 10px;
}
.inbox_chat { height: 550px; overflow-y: scroll;}

.active_chat{ background:#ebebeb;}

.incoming_msg_img {
  display: inline-block;
  width: 6%;
}
.received_msg {
  display: inline-block;
  padding: 0 0 0 10px;
  vertical-align: top;
  width: 92%;
 }
 .received_withd_msg p {
  background: #ebebeb none repeat scroll 0 0;
  border-radius: 3px;
  color: #646464;
  font-size: 14px;
  margin: 0;
  padding: 5px 10px 5px 12px;
  width: 100%;
}
.time_date {
  color: #747474;
  display: block;
  font-size: 12px;
  margin: 8px 0 0;
}
.received_withd_msg { width: 57%;}
.mesgs {
  float: left;
  padding: 30px 15px 0 25px;
  width: 60%;
}

 .sent_msg p {
  background: #05728f none repeat scroll 0 0;
  border-radius: 3px;
  font-size: 14px;
  margin: 0; color:#fff;
  padding: 5px 10px 5px 12px;
  width:100%;
}
.outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
.sent_msg {
  float: right;
  width: 46%;
}
.input_msg_write input {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  color: #4c4c4c;
  font-size: 15px;
  min-height: 48px;
  width: 100%;
}

.type_msg {border-top: 1px solid #c4c4c4;position: relative;}
.msg_send_btn {
  background: #05728f none repeat scroll 0 0;
  border: medium none;
  border-radius: 50%;
  color: #fff;
  cursor: pointer;
  font-size: 17px;
  height: 33px;
  position: absolute;
  right: 0;
  top: 11px;
  width: 33px;
}
.messaging { padding: 0 0 50px 0;}
.msg_history {
  height: 516px;
  overflow-y: auto;
}
</style>
</head>
<body>
<div class="container">
<h3 class=" text-center">Messaging</h3>
<div class="messaging">
      <div class="inbox_msg">
        <div class="inbox_people">
          <div class="headind_srch">
            <div class="recent_heading">
              <h4>User List</h4>
            </div>
            <div class="srch_bar">
              <div class="stylish-input-group">
                <input type="text" class="search-bar" onkeyup="getConnectedUserList()"  placeholder="Search" >
                <span class="input-group-addon">
                <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                </span> </div>
            </div>
          </div>
          <div class="inbox_chat">
            
            
            
          </div>
        </div>
        <div class="mesgs">
          <div class="msg_history">
         
          </div>
		  <form id="send_message_form">
           <div class="type_msg">
            <div class="input_msg_write">
              <input type="text" class="write_msg" placeholder="Type a message" />
              <button class="msg_send_btn" onclick="sendMessage()" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
            </div>
			<span id="message-error" class="invalid-feedback alert-danger" style="display: none;">The message field is required.</span>
           </div>
		  </form>
        </div>
      </div>
      
       <input  type="hidden" id="message_room" value="">
       <input  type="hidden" id="room" value="">
	   <input  type="hidden" id="to_user" value="">
      
    </div>
	</div>
	
	<script>
	var loginUserId;
	$(document).ready(function(){
	    loginUserId = localStorage.getItem('userId');
		getUserList(loginUserId);
		  /*var profile = localStorage.getItem('loginUserProfile');
	    var profile = localStorage.getItem('loginUserName');
		console.log(name)
	   $('#user_profile').attr("src",profile);
	   $('#user_name').html('<h5 >'+name+'</h5>')*/
			alert('hi');
	});
   function getUserList(loginUserId){

	   $.ajax({
            url:"http://localhost:8080/api/chat/user-list" ,
			data:{user_id :loginUserId,search:$('.search-bar').val()},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
				$('.inbox_chat').html('');
				var str = '';
				for (var i=0;i<result.data.length; i++){
					
					
					str = str + " <div  class='chat_list'>"+
						   "<a href='javascript:void(0)' onclick = 'getUserMessage()'>  "+
							 "<div class='chat_people'>"+
							 "<div class='chat_img'> <img src='"+result.data[i].profile+"' alt='sunil'> </div>"+
							 "<div class='chat_ib'>"+
							 "<h5>"+result.data[i].name+"<span class='chat_date'>"+result.data[i].created_at+"</span></h5>"+
							 "<p>"+result.data[i].email+"</p>"+
							"</div></div></a></div>";
              
				}
				$('.inbox_chat').html(str);
				$('#connect_row_'+firstRoomId).addClass('active_chat');
            }
            
        });
	}
	
	function getUserMessage(loginUserId,firstRoomId,room){
		 console.log('room'+room)
		 $('#message_room').val(firstRoomId);
		 $('#room').val(room);
		 $('#to_user').val($('#to_user'+firstRoomId).val())
		 socket.emit('new-user', room, loginUserId)
		 $.ajax({
            url:"http://localhost:8080/api/chat/user-message" ,
			data:{user_id :loginUserId,room_id:firstRoomId},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
				$('.msg_history').html('');
				var str = '';
				var allMessages = result.data.reverse();
				for (var i=0;i < allMessages.length; i++){
					if(result.data[i].user_id != loginUserId){
					str = str + " <div class='incoming_msg'>"+
				   "<div class='incoming_msg_img'> <img src='"+result.data[i].user.profile+"' alt='sunil'> </div>"+
				   "<div class='received_msg'>"+
					"<div class='received_withd_msg'>"+
					  "<p>"+result.data[i].message+"</p>"+
                   "  <span class='time_date'> "+result.data[i].created_at+"</span></div></div></div>" ;
					}else{
					str = str + "<div class='outgoing_msg'><div class='sent_msg'><p>"+result.data[i].message+"</p>"+
				   "<span class='time_date'>"+result.data[i].created_at+"</span> </div></div>" };
				
				}
				$('.msg_history').html(str);
				$('.chat_list').removeClass('active_chat');
				$('#connect_row_'+firstRoomId).addClass('active_chat');
				$(".msg_history").animate({ scrollTop: 6000 }, "slow");
				
            }
            
        });
	}
	
	function sendMessage(toUser){
		console.log(socket.id);
		var loginUserId = localStorage.getItem('userId');
		var message = $('.write_msg').val();
		if(message == ''){
			$('#message-error').show();
		}else{
			$('#message-error').hide();
			
			$.ajax({
            url:"http://localhost:8080/api/chat/send-message" ,
			data:{from_user:loginUserId,room_id:$('#message_room').val(),message:$('.write_msg').val(),to_user:$('#to_user').val()},
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
				var roomName = $('#room').val();
				var message = $('.write_msg').val();
				socket.emit('send-chat-message', roomName, result.data)
				$('.write_msg').val('');
				var str = "<div class='outgoing_msg'><div class='sent_msg'><p>"+result.data.message+"</p><span class='time_date'>"+result.data.created_at+"</span> </div></div>";
				$('.msg_history').append(str);
				$(".msg_history").animate({ scrollTop: 6000 }, "slow");
				getConnectedUserList(loginUserId);
            }
            
        });
		}
	}
	

</script
 
<?php 

include("./common-page/footer.php");

?>